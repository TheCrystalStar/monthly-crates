package com.shadebyte.monthlycrates;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.shadebyte.monthlycrates.commands.MonthlyCratesCMD;
import com.shadebyte.monthlycrates.events.CrateEvents;
import com.shadebyte.monthlycrates.utilities.ConfigWrapper;

public class Core extends JavaPlugin {

	public static final String VERSION = "1.0.0";
	public HashMap<UUID, String> openingCrate = new HashMap<>();
	
	private static Core instance;
	private static ConfigWrapper crateConfig;

	@Override
	public void onEnable() {
		instance = this;
		crateConfig = new ConfigWrapper(this, "", "Crates.yml");
		crateConfig.saveConfig();
		crateConfig.reloadConfig();
		
		Bukkit.getPluginManager().registerEvents(new CrateEvents(), this);
		
		getCommand("Monthlycrates").setExecutor(new MonthlyCratesCMD());
	}

	@Override
	public void onDisable() {
		instance = null;
	}

	public static Core getInstance() {
		return instance;
	}

	public static ConfigWrapper getCrateConfig() {
		return crateConfig;
	}
}
