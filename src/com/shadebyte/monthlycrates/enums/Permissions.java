package com.shadebyte.monthlycrates.enums;

public enum Permissions {

	BASE("MonthlyCrates.cmd"),
	CREATE(Permissions.BASE.getNode() + ".create"),
	DELETE(Permissions.BASE.getNode() + ".delete"),
	GIVE(Permissions.BASE.getNode() + ".give"),
	GIVEALL(Permissions.BASE.getNode() + ".giveall");
	
	private String node;
	
	private Permissions(String node) {
		this.node = node;
	}
	
	public String getNode() {
		return this.node;
	}
}
