package com.shadebyte.monthlycrates.enums;

import org.bukkit.ChatColor;

import com.shadebyte.monthlycrates.Core;

public enum Lang {

	CREATE(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.create"))),
	DELETE(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.delete"))),
	DONT_EXIST(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.dontexist"))),
	ALREADY_EXIST(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.alreadyexist"))),
	GIVE(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.give"))),
	PLAYER_OFFLINE(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.playeroffline"))),
	NOTANUMBER(ChatColor.translateAlternateColorCodes('&', Core.getInstance().getConfig().getString("messages.notanumber")));
	
	private String node;
	
	private Lang(String node) {
		this.node = node;
	}
	
	public String getNode() {
		return this.node;
	}
}
