package com.shadebyte.monthlycrates.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public abstract class ShadeCommand implements CommandExecutor {

	private final String command;
	private final String permission;

	protected ShadeCommand(String command, String permission) {
		this.command = command;
		this.permission = permission;
	}

	public String getName() {
		return command;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!sender.hasPermission(permission)) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou do not have permission to use that command!"));
			return true;
		}

		execute(sender, args);
		return true;
	}

	public abstract void execute(CommandSender sender, String[] args);
}
