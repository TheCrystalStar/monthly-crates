package com.shadebyte.monthlycrates.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.shadebyte.monthlycrates.MCrateAPI;
import com.shadebyte.monthlycrates.commands.SubCommand;
import com.shadebyte.monthlycrates.enums.Lang;
import com.shadebyte.monthlycrates.enums.Permissions;

public class CreateCMD extends SubCommand {

	public CreateCMD() {
		super("create", "", Permissions.CREATE.getNode(), "", 2);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (args.length == 1) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b/MCrate create &c<name>"));
		}

		if (args.length == 2) {
			if (MCrateAPI.getInstance().doesMonthlyCrateExist(args[1])) {
				sender.sendMessage(Lang.ALREADY_EXIST.getNode().replace("{name}", args[1]));
			} else {
				MCrateAPI.getInstance().createNewMonthlyCrate(args[1]);
				sender.sendMessage(Lang.CREATE.getNode().replace("{name}", args[1]));
			}
		}
	}
}
