package com.shadebyte.monthlycrates.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.shadebyte.monthlycrates.MCrateAPI;
import com.shadebyte.monthlycrates.commands.SubCommand;
import com.shadebyte.monthlycrates.enums.Lang;
import com.shadebyte.monthlycrates.enums.Permissions;

public class DeleteCMD extends SubCommand {

	public DeleteCMD() {
		super("delete", "", Permissions.DELETE.getNode(), "", 2);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (args.length == 1) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b/MCrate delete &c<name>"));
		}

		if (args.length == 2) {
			if (!MCrateAPI.getInstance().doesMonthlyCrateExist(args[1])) {
				sender.sendMessage(Lang.DONT_EXIST.getNode().replace("{name}", args[1]));
			} else {
				MCrateAPI.getInstance().removeMonthlyCrate(args[1]);
				sender.sendMessage(Lang.DELETE.getNode().replace("{name}", args[1]));
			}
		}
	}
}
