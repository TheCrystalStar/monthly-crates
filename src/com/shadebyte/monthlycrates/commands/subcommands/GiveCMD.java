package com.shadebyte.monthlycrates.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.shadebyte.monthlycrates.MCrateAPI;
import com.shadebyte.monthlycrates.commands.SubCommand;
import com.shadebyte.monthlycrates.enums.Lang;
import com.shadebyte.monthlycrates.enums.Permissions;

public class GiveCMD extends SubCommand {

	public GiveCMD() {
		super("give", "", Permissions.GIVE.getNode(), "", 2);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		if (args.length == 1) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b/MCrate give &c<Player> <Crate> <#>"));
		}

		if (args.length == 2) {
			Player p = Bukkit.getPlayerExact(args[1]);
			if (p == null) {
				sender.sendMessage(Lang.PLAYER_OFFLINE.getNode());
				return;
			}
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b/MCrate give " + args[1] + " &c<Crate> <#>"));
		}

		if (args.length == 3) {
			Player p = Bukkit.getPlayerExact(args[1]);
			if (p == null) {
				sender.sendMessage(Lang.PLAYER_OFFLINE.getNode());
				return;
			}

			if (!MCrateAPI.getInstance().doesMonthlyCrateExist(args[2])) {
				sender.sendMessage(Lang.DONT_EXIST.getNode().replace("{name}", args[2]));
				return;
			}
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b/MCrate give " + args[1] + " " + args[2] + " &c<#>"));
		}

		if (args.length == 4) {
			Player p = Bukkit.getPlayerExact(args[1]);
			if (p == null) {
				sender.sendMessage(Lang.PLAYER_OFFLINE.getNode());
				return;
			}

			if (!MCrateAPI.getInstance().doesMonthlyCrateExist(args[2])) {
				sender.sendMessage(Lang.DONT_EXIST.getNode().replace("{name}", args[2]));
				return;
			}

			if (!MCrateAPI.getInstance().isInteger(args[3])) {
				sender.sendMessage(Lang.NOTANUMBER.getNode());
				return;
			}

			for (int i = 0; i < Integer.parseInt(args[3]); i++) {
				p.getInventory().addItem(MCrateAPI.getInstance().makeMonthlyCrate(args[2], p));
			}
			sender.sendMessage(Lang.GIVE.getNode().replace("{player}", p.getName()).replace("{name}", args[2]).replace("{amount}", args[3]));
		}
	}
}
