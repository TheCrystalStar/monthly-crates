package com.shadebyte.monthlycrates.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;

import com.shadebyte.monthlycrates.commands.subcommands.CreateCMD;
import com.shadebyte.monthlycrates.commands.subcommands.DeleteCMD;
import com.shadebyte.monthlycrates.commands.subcommands.GiveCMD;
import com.shadebyte.monthlycrates.enums.Permissions;

public class MonthlyCratesCMD extends ShadeCommand {

	private final Map<String, SubCommand> children = new HashMap<>();

	public MonthlyCratesCMD() {
		super("MonthlyCrates", Permissions.BASE.getNode());
		children.put("create", new CreateCMD());
		children.put("delete", new DeleteCMD());
		children.put("give", new GiveCMD());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {

		
		
		SubCommand child = children.get(args[0].toLowerCase());

		if (child != null) {
			child.execute(sender, args);
			return;
		}
	}

	public boolean registerChildren(SubCommand command) {

		if (children.get(command.getName()) != null) {
			return false;
		}

		children.put(command.getName(), command);
		return true;
	}

}
