package com.shadebyte.monthlycrates;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.shadebyte.monthlycrates.utilities.ItemUtil;

@SuppressWarnings("resource")
public class MCrateAPI {

	/**
	 * Create a private instance of the NeptuneAPI class.
	 */
	private static MCrateAPI instance = new MCrateAPI();

	/**
	 * Get the private instance of the NeptuneAPI class.
	 */
	public static MCrateAPI getInstance() {
		return instance;
	}

	/**
	 * Checks if the given number is an integer.
	 */
	public boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	/**
	 * @param resourceID
	 *            is the id of the resource, this is used to get the latest version
	 *            of the plugin on spigot, then compares it to Monthly Crate's
	 *            version to determine if an update is required for the plugin or
	 *            not.
	 */
	public boolean needsUpdate(int resourceID) {
		String link = "https://api.spigotmc.org/legacy/update.php?resource=" + resourceID;
		String version = "";
		try {
			version = new Scanner(new URL(link).openStream(), "UTF-8").useDelimiter("\\A").next();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!Core.VERSION.equals(version)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param resourceID
	 *            is used to check the plugin version on spigot, this method only
	 *            returns a string of the latest verison off spigot.
	 */
	public String getNewestVersion(int resourceID) {
		String link = "https://api.spigotmc.org/legacy/update.php?resource=" + resourceID;
		String version = "";
		try {
			version = new Scanner(new URL(link).openStream(), "UTF-8").useDelimiter("\\A").next();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return version;
	}

	/**
	 * @param name
	 *            is the name of the monthly crate of which the plugin will check is
	 *            it already exist, and return true or false.
	 */
	public boolean doesMonthlyCrateExist(String name) {
		return Core.getCrateConfig().getConfig().contains("crates." + name.toLowerCase());
	}

	/**
	 * @param name
	 *            is the name given to the monthly crate, this method will create a
	 *            monthly crate and add it to the config.
	 */
	public void createNewMonthlyCrate(String name) {
		// Check if the crate exist.
		if (doesMonthlyCrateExist(name)) {
			return;
		}
		// Create the monthly crate, and add it to the data file.
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".name", name);
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".id", name.toLowerCase());
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".title", "&6&l" + name + " &e&Crate");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".crateitem.item", "ENDER_CHEST:0");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".crateitem.name", "&6&l" + name + " &E&lCrate");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".crateitem.lore", Arrays.asList("", "&eUnlocked by {player}", "", "&6Text", "&eText", "&bText", "&9Text"));
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".unrolledslot.item", "STAINED_GLASS_PANE:14");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".unrolledslot.name", "&6&l???");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".unrolledslot.lore", Arrays.asList("&7???"));
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".filler.item", "STAINED_GLASS_PANE:0");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".filler.name", "&7Click a red pane");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".filler.lore", Arrays.asList("&7???"));
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".middle.item", "STAINED_GLASS_PANE:14");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".middle.name", "&6&lUncover your middle item");
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase() + ".middle.lore", Arrays.asList("&7???"));
		Core.getCrateConfig().saveConfig();
	}

	/**
	 * @param name
	 *            is the name given to see if a monthly crate exist if so, then
	 *            delete it from the config.
	 */
	public void removeMonthlyCrate(String name) {
		// Check if the crate exist.
		if (!doesMonthlyCrateExist(name)) {
			return;
		}
		// Delete the monthly crate from the data file.
		Core.getCrateConfig().getConfig().set("crates." + name.toLowerCase(), null);
		Core.getCrateConfig().saveConfig();
	}

	/**
	 * Used to create the monthly crate item.
	 */
	public ItemStack makeMonthlyCrate(String name, Player p) {
		String rawItem = Core.getCrateConfig().getConfig().getString("crates." + name.toLowerCase() + ".crateitem.item");
		String item[] = rawItem.split(":");
		ItemStack is = new ItemStack(Material.valueOf(item[0]), 1, Short.parseShort(item[1]));
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Core.getCrateConfig().getConfig().getString("crates." + name.toLowerCase() + ".crateitem.name")));
		ArrayList<String> lore = new ArrayList<>();
		for (String all : Core.getCrateConfig().getConfig().getStringList("crates." + name.toLowerCase() + ".crateitem.lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', all.replace("{player}", p.getName())));
		}
		meta.setLore(lore);
		is.setItemMeta(meta);
		ItemStack x = ItemUtil.setTag(is, "MonthlyCrate", name.toLowerCase());
		return x;
	}

	/**
	 * This method is used to check if the item the player is holding in their hand
	 * is a monthly crate, if so it returns the monthly crate id.
	 */
	public String getCrateID(ItemStack is) {
		if (ItemUtil.getTag(is, "MonthlyCrate") == null) {
			return null;
		}
		return (String) ItemUtil.getTag(is, "MonthlyCrate");
	}

	/**
	 * Used to get a stained glass pane.
	 */
	public ItemStack getColorPane(int color) {
		return new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) color);
	}

	/**
	 * Method to create a itemstack of a certain crate node.
	 */
	public ItemStack createCrateItemStack(String crate, String node) {
		String itemr = Core.getCrateConfig().getConfig().getString("crates." + crate.toLowerCase() + "." + node.toLowerCase() + ".item");
		String item[] = itemr.split(":");
		ItemStack is = new ItemStack(Material.getMaterial(item[0]), 1, Short.parseShort(item[1]));
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Core.getCrateConfig().getConfig().getString("crates." + crate.toLowerCase() + "." + node.toLowerCase() + ".name")));
		ArrayList<String> lore = new ArrayList<>();
		for (String all : Core.getCrateConfig().getConfig().getStringList("crates." + crate.toLowerCase() + "." + node.toLowerCase() + ".lore")) {
			lore.add(ChatColor.translateAlternateColorCodes('&', all));
		}
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}

	/**
	 * Create the Monthly Crate GUI with it's items and filler items.
	 */
	public Inventory getCrateMenu(String crate) {
		Inventory inv = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', Core.getCrateConfig().getConfig().getString("crates." + crate.toLowerCase() + ".title")));
		for (int i = 0; i < 54; i++) {
			inv.setItem(inv.firstEmpty(), createCrateItemStack(crate, "filler"));
		}
		inv.setItem(12, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(13, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(14, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(21, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(22, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(23, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(30, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(31, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(32, createCrateItemStack(crate, "unrolledslot"));
		inv.setItem(49, createCrateItemStack(crate, "middle"));
		return inv;
	}
}
