package com.shadebyte.monthlycrates.events.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CratePlaceAttemptEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	private Player p;
	private String crateID;

	public CratePlaceAttemptEvent(Player p, String crateID) {
		this.p = p;
		this.crateID = crateID;
	}

	public Player getPlayer() {
		return this.p;
	}

	public String getCrateID() {
		return this.crateID;
	}
}
