package com.shadebyte.monthlycrates.utilities;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import com.shadebyte.monthlycrates.Core;
import com.shadebyte.monthlycrates.MCrateAPI;

public class Animation {

    public static ArrayList<UUID> slotOne = new ArrayList<>();
    public static ArrayList<UUID> slotTwo = new ArrayList<>();
    public static ArrayList<UUID> slotThree = new ArrayList<>();
    public static ArrayList<UUID> slotFour = new ArrayList<>();
    public static ArrayList<UUID> slotFive = new ArrayList<>();
    public static ArrayList<UUID> slotSix = new ArrayList<>();
    public static ArrayList<UUID> slotSeven = new ArrayList<>();
    public static ArrayList<UUID> slotEight = new ArrayList<>();
    public static ArrayList<UUID> slotNine = new ArrayList<>();

    public boolean slotOneFinished = false;
    public boolean slotTwoFinished = false;
    public boolean slotThreeFinished = false;
    public boolean slotFourFinished = false;
    public boolean slotFiveFinished = false;
    public boolean slotSixFinished = false;
    public boolean slotSevenFinished = false;
    public boolean slotEightFinished = false;
    public boolean slotNineFinished = false;

    private final int[] ROLL_SLOT_ONE = new int[]{3, 9, 10, 11, 15, 16, 17, 39, 48};
    private final int[] ROLL_SLOT_TWO = new int[]{4, 9, 10, 11, 15, 16, 17, 40};
    private final int[] ROLL_SLOT_THREE = new int[]{5, 9, 10, 11, 15, 16, 17, 39, 41, 50};
    private final int[] ROLL_SLOT_FOUR = new int[]{3, 18, 19, 20, 24, 25, 26, 39, 48};
    private final int[] ROLL_SLOT_FIVE = new int[]{4, 18, 19, 20, 24, 25, 26, 40};
    private final int[] ROLL_SLOT_SIX = new int[]{5, 18, 19, 20, 24, 25, 26, 41, 50};
    private final int[] ROLL_SLOT_SEVEN = new int[]{3, 39, 48, 27, 28, 29, 33, 34, 35};
    private final int[] ROLL_SLOT_EIGHT = new int[]{4, 40, 27, 28, 29, 33, 34, 35};
    private final int[] ROLL_SLOT_NINE = new int[]{5, 41, 50, 27, 28, 29, 33, 34, 35};

    private Player p;
    private Inventory inv;

    public Animation(Player p, Inventory inv) {
        this.p = p;
        this.inv = inv;
    }

    public Player getPlayer() {
        return this.p;
    }

    public Inventory getInventory() {
        return this.inv;
    }

    public void rollSlot(int slot) {
        new BukkitRunnable() {
            int x = 0;

            @Override
            public void run() {
                switch (slot) {
                    case 1:
                        for (int i : ROLL_SLOT_ONE) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 2:
                        for (int i : ROLL_SLOT_TWO) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 3:
                        for (int i : ROLL_SLOT_THREE) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 4:
                        for (int i : ROLL_SLOT_FOUR) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 5:
                        for (int i : ROLL_SLOT_FIVE) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 6:
                        for (int i : ROLL_SLOT_SIX) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 7:
                        for (int i : ROLL_SLOT_SEVEN) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 8:
                        for (int i : ROLL_SLOT_EIGHT) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                    case 9:
                        for (int i : ROLL_SLOT_NINE) {
                            inv.setItem(i, MCrateAPI.getInstance().getColorPane(ThreadLocalRandom.current().nextInt(0, 14 + 1)));
                        }
                        x++;
                        break;
                }

                if (x == 70) {
                    switch (slot) {
                        case 1:
                            slotOneFinished = true;
                            slotOne.add(p.getUniqueId());
                            break;
                        case 2:
                            slotTwoFinished = true;
                            slotTwo.add(p.getUniqueId());
                            break;
                        case 3:
                            slotThreeFinished = true;
                            slotThree.add(p.getUniqueId());
                            break;
                        case 4:
                            slotFourFinished = true;
                            slotFour.add(p.getUniqueId());
                            break;
                        case 5:
                            slotFiveFinished = true;
                            slotFive.add(p.getUniqueId());
                            break;
                        case 6:
                            slotSixFinished = true;
                            slotSix.add(p.getUniqueId());
                            break;
                        case 7:
                            slotSevenFinished = true;
                            slotSeven.add(p.getUniqueId());
                            break;
                        case 8:
                            slotEightFinished = true;
                            slotEight.add(p.getUniqueId());
                            break;
                        case 9:
                            slotNineFinished = true;
                            slotNine.add(p.getUniqueId());
                            break;
                    }
                    this.cancel();
                }
            }
        }.runTaskTimer(Core.getInstance(), 0, 3);
    }

    public boolean allNineSlotsFinished(Player p) {
        boolean x = false;
        if (slotOne.contains(p.getUniqueId()) && slotTwo.contains(p.getUniqueId()) && slotThree.contains(p.getUniqueId()) &&
                slotFour.contains(p.getUniqueId()) && slotFive.contains(p.getUniqueId()) && slotSix.contains(p.getUniqueId()) &&
                slotSeven.contains(p.getUniqueId()) && slotNine.contains(p.getUniqueId()) && slotNine.contains(p.getUniqueId())) {
            x = true;
        }
        return x;
    }

    public void getRandomItemFromCrateSlot(int slot) {

    }
}
