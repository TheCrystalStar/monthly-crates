package com.shadebyte.monthlycrates.utilities;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

public class ItemUtil {
	
	private static HashMap<String, Class<?>> classCache;
	private static HashMap<String, Method> methodCache;
	private static HashMap<Class<?>, Constructor<?>> constructorCache;
	private static HashMap<Class<?>, Class<?>> NBTClasses;
	private static HashMap<Class<?>, Field> NBTTagFieldCache;
	private static String version;

	static {
		version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];

		classCache = new HashMap<String, Class<?>>();
		try {
			classCache.put("NBTBase", Class.forName("net.minecraft.server." + version + "." + "NBTBase"));
			classCache.put("NBTTagCompound", Class.forName("net.minecraft.server." + version + "." + "NBTTagCompound"));
			classCache.put("ItemStack", Class.forName("net.minecraft.server." + version + "." + "ItemStack"));
			classCache.put("CraftItemStack", Class.forName("org.bukkit.craftbukkit." + version + ".inventory." + "CraftItemStack"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		NBTClasses = new HashMap<Class<?>, Class<?>>();
		try {
			NBTClasses.put(Byte.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagByte"));
			NBTClasses.put(String.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagString"));
			NBTClasses.put(Double.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagDouble"));
			NBTClasses.put(Integer.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagInt"));
			NBTClasses.put(Long.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagLong"));
			NBTClasses.put(Short.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagShort"));
			NBTClasses.put(Float.class, Class.forName("net.minecraft.server." + version + "." + "NBTTagFloat"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		methodCache = new HashMap<String, Method>();
		try {
			methodCache.put("hasTag", getNMSClass("ItemStack").getMethod("hasTag"));
			methodCache.put("getTag", getNMSClass("ItemStack").getMethod("getTag"));
			methodCache.put("setTag", getNMSClass("ItemStack").getMethod("setTag", getNMSClass("NBTTagCompound")));
			methodCache.put("get", getNMSClass("NBTTagCompound").getMethod("get", String.class));
			methodCache.put("set", getNMSClass("NBTTagCompound").getMethod("set", String.class, getNMSClass("NBTBase")));
			methodCache.put("asNMSCopy", getNMSClass("CraftItemStack").getMethod("asNMSCopy", ItemStack.class));
			methodCache.put("asBukkitCopy", getNMSClass("CraftItemStack").getMethod("asBukkitCopy", getNMSClass("ItemStack")));
		} catch (Exception e) {
			e.printStackTrace();
		}

		constructorCache = new HashMap<Class<?>, Constructor<?>>();
		try {
			constructorCache.put(getNBTTag(Byte.class), getNBTTag(Byte.class).getConstructor(byte.class));
			constructorCache.put(getNBTTag(String.class), getNBTTag(String.class).getConstructor(String.class));
			constructorCache.put(getNBTTag(Double.class), getNBTTag(Double.class).getConstructor(double.class));
			constructorCache.put(getNBTTag(Integer.class), getNBTTag(Integer.class).getConstructor(int.class));
			constructorCache.put(getNBTTag(Long.class), getNBTTag(Long.class).getConstructor(long.class));
		} catch (Exception e) {
			e.printStackTrace();
		}

		NBTTagFieldCache = new HashMap<Class<?>, Field>();
		try {
			for (Class<?> clazz : NBTClasses.values()) {
				Field data = clazz.getDeclaredField("data");
				data.setAccessible(true);
				NBTTagFieldCache.put(clazz, data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Class<?> getPrimitiveClass(Class<?> clazz) {
		if (clazz.getSimpleName().equals("Byte"))
			return byte.class;
		if (clazz.getSimpleName().equals("Short"))
			return short.class;
		if (clazz.getSimpleName().equals("Integer"))
			return int.class;
		if (clazz.getSimpleName().equals("Long"))
			return long.class;
		if (clazz.getSimpleName().equals("Character"))
			return char.class;
		if (clazz.getSimpleName().equals("Float"))
			return float.class;
		if (clazz.getSimpleName().equals("Double"))
			return double.class;
		if (clazz.getSimpleName().equals("Boolean"))
			return boolean.class;
		if (clazz.getSimpleName().equals("Void"))
			return void.class;
		return clazz;
	}

	public static Class<?> getNBTTag(Class<?> primitiveType) {
		if (NBTClasses.containsKey(primitiveType))
			return NBTClasses.get(primitiveType);
		return primitiveType;
	}

	public static Object getNBTVar(Object object) {
		if (object == null)
			return null;
		Class<?> clazz = object.getClass();
		try {
			if (NBTTagFieldCache.containsKey(clazz)) {
				return NBTTagFieldCache.get(clazz).get(object);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public static Method getMethod(String name) {
		return methodCache.containsKey(name) ? methodCache.get(name) : null;

	}

	public static Constructor<?> getConstructor(Class<?> clazz) {
		return constructorCache.containsKey(clazz) ? constructorCache.get(clazz) : null;
	}

	public static Class<?> getNMSClass(String name) {
		if (classCache.containsKey(name)) {
			return classCache.get(name);
		}

		try {
			return Class.forName("net.minecraft.server." + version + "." + name);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Gets an NBT tag in a given item
	 *
	 * @param item
	 *            The itemstack to get the key from
	 * @param key
	 *            The key to fetch
	 * @return Null if it does not exist
	 */
	public static Object getTag(ItemStack item, String key) {
		try {
			Object stack = null;
			stack = getMethod("asNMSCopy").invoke(null, item);

			Object tag = null;

			if (getMethod("hasTag").invoke(stack).equals(true)) {
				tag = getMethod("getTag").invoke(stack);
			} else {
				tag = getNMSClass("NBTTagCompound").newInstance();
			}

			Object notCompound = getMethod("get").invoke(tag, key);
			return getNBTVar(notCompound);
		} catch (Exception exception) {
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * Sets an NBT tag in an item with the provided key and value
	 *
	 * @param item
	 *            The itemstack to set
	 * @param key
	 *            The key
	 * @param value
	 *            The value
	 * @return A new ItemStack with the updated NBT tags
	 */
	public static ItemStack setTag(ItemStack item, String key, Object value) {
		try {
			Object stack = getMethod("asNMSCopy").invoke(null, item);

			Object tag = null;

			if (getMethod("hasTag").invoke(stack).equals(true)) {
				tag = getMethod("getTag").invoke(stack);
			} else {
				tag = getNMSClass("NBTTagCompound").newInstance();
			}

			Object notCompound = getConstructor(getNBTTag(value.getClass())).newInstance(value);

			getMethod("set").invoke(tag, key, notCompound);
			getMethod("setTag").invoke(stack, tag);
			return (ItemStack) getMethod("asBukkitCopy").invoke(null, stack);
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}
}